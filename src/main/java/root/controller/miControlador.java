/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import root.dao.TblPersonasJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.entity.TblPersonas;

/**
 *
 * @author Karukami
 */
@WebServlet(name = "miControlador", urlPatterns = {"/miControlador"})
public class miControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet miControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet miControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // DAO y Entity
        TblPersonasJpaController dao = new TblPersonasJpaController();
        TblPersonas personaNueva = new TblPersonas();
        
        // BOTON INGRESAR - MENU PRINCIPAL
        if(request.getParameter("btn_jsp_ingresar") != null){
            request.getRequestDispatcher("ingresar.jsp").forward(request, response);
        // BOTON CONFIRMAR INSERT
        } else if (request.getParameter("btn_ingresar") != null){
            
            // Recepción de data
            String rut = request.getParameter("txt_rut");
            String nombre = request.getParameter("txt_nombre");
            String apellido = request.getParameter("txt_apellido");
            String direccion = request.getParameter("txt_direccion");
            String ciudad = request.getParameter("txt_ciudad");
            
            personaNueva.setRut(rut);
            personaNueva.setNombre(nombre);
            personaNueva.setApellido(apellido);
            personaNueva.setDireccion(direccion);
            personaNueva.setCiudad(ciudad);

            try {
                dao.create(personaNueva);
            } catch (Exception ex) {
                Logger.getLogger(miControlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            mensajeInsert(request, response);
            
        } else if (request.getParameter("btn_jsp_listar") != null){
            
            /**********/
            /* LISTAR */
            /**********/
            System.out.println("SELECCIONASTE EL BOTON LISTAR");
            
            // Lista para retorno BD
            List <TblPersonas> listaPersonas;

            // Creación de Entity Manager
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager em = emf.createEntityManager();

            try{
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(TblPersonas.class));
                Query q = em.createQuery(cq);

                listaPersonas = q.getResultList();
                
                mostrarLista(request, response, listaPersonas);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            } 
            finally {
                em.close();
            }
            
        } else if (request.getParameter("btn_jsp_eliminar") != null) {
            List <TblPersonas> listaPersonas = null;

            // Creación de Entity Manager
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager em = emf.createEntityManager();

            try{
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(TblPersonas.class));
                Query q = em.createQuery(cq);

                listaPersonas = q.getResultList();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            } 
            finally {
                em.close();
            }
            
            request.setAttribute("listaPersonasJsp",listaPersonas);
            request.getRequestDispatcher("eliminar.jsp").forward(request, response);
        } else if (request.getParameter("btn_eliminar") != null) {
            
            // Recepcion de data
            String rutborrar = request.getParameter("select_eliminar");
            
            try {
                dao.destroy(rutborrar);
            } catch (Exception ex) {
                Logger.getLogger(miControlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            mensajeDelete(request, response);
            
        } else if (request.getParameter("btn_jsp_editar") != null) {
            List <TblPersonas> listaPersonas = null;

            // Creación de Entity Manager
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager em = emf.createEntityManager();

            try{
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(TblPersonas.class));
                Query q = em.createQuery(cq);

                listaPersonas = q.getResultList();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            } 
            finally {
                em.close();
            }
            request.setAttribute("listaPersonasJsp",listaPersonas);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        } else if (request.getParameter("btn_editar") != null) {
            
            String ruteditar = request.getParameter("select_editar");
            
            // Recepción de data
            String nombre = request.getParameter("txt_edit_nombre");
            String apellido = request.getParameter("txt_edit_apellido");
            String direccion = request.getParameter("txt_edit_direccion");
            String ciudad = request.getParameter("txt_edit_ciudad");
            
            personaNueva.setRut(ruteditar);
            personaNueva.setNombre(nombre);
            personaNueva.setApellido(apellido);
            personaNueva.setDireccion(direccion);
            personaNueva.setCiudad(ciudad);
            
            try {
                dao.edit(personaNueva);
            } catch (Exception ex) {
                Logger.getLogger(miControlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            mensajeEdit(request, response);
        }
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void mostrarLista(HttpServletRequest request, HttpServletResponse response, List<TblPersonas> listaPersonas)
        throws ServletException, IOException{
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Retorno BD</title>");
            out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                        "        <style>\n" +
                        "            body {\n" +
                        "                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);\n" +
                        "                font-family: sans-serif;\n" +
                        "            }\n" +
                        "            \n" +
                        "            #divPrincipal {\n" +
                        "                background-color: white;\n" +
                        "                margin-top: 30px;\n" +
                        "                padding: 30px;\n" +
                        "                border-radius: 10px;\n" +
                        "            }\n" +
                        "        </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-5\" id=\"divPrincipal\">");
            out.println("<h4>Tabla tbl_personas en Base de datos:</h4>");
            out.println("<hr>");
            out.println("<table class='table table-bordered table-sm table-hover'>");
            out.println("<tr>");
            out.println("<thead>");
            out.println("<th>RUT</th>");
            out.println("<th>NOMBRE</th>");
            out.println("<th>APELLIDO</th>");
            out.println("<th>DIRECCION</th>");
            out.println("<th>CIUDAD</th>");
            out.println("</thead>");
            out.println("</tr>");
            for(int i = 0; i < listaPersonas.size(); i++){
                out.println("<tr>");
                out.println("<td>" + listaPersonas.get(i).getRut() + "</td>");
                out.println("<td>" + listaPersonas.get(i).getNombre() + "</td>");
                out.println("<td>" + listaPersonas.get(i).getApellido() + "</td>");
                out.println("<td>" + listaPersonas.get(i).getDireccion() + "</td>");
                out.println("<td>" + listaPersonas.get(i).getCiudad() + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.back();\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    protected void mensajeInsert(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>INSERT</title>");
             out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                        "        <style>\n" +
                        "            body {\n" +
                        "                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);\n" +
                        "                font-family: sans-serif;\n" +
                        "            }\n" +
                        "            \n" +
                        "            #divPrincipal {\n" +
                        "                background-color: white;\n" +
                        "                margin-top: 30px;\n" +
                        "                padding: 30px;\n" +
                        "                border-radius: 10px;\n" +
                        "                text-align: center;\n" +
                        "            }\n" +
                        "        </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-5\" id=\"divPrincipal\">");
            out.println("<h1>Inserción realizada</h1>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.go(-2);\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    protected void mensajeDelete(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>DELETE</title>");
             out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                        "        <style>\n" +
                        "            body {\n" +
                        "                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);\n" +
                        "                font-family: sans-serif;\n" +
                        "            }\n" +
                        "            \n" +
                        "            #divPrincipal {\n" +
                        "                background-color: white;\n" +
                        "                margin-top: 30px;\n" +
                        "                padding: 30px;\n" +
                        "                border-radius: 10px;\n" +
                        "                text-align: center;\n" +
                        "            }\n" +
                        "        </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-5\" id=\"divPrincipal\">");
            out.println("<h1>Eliminación realizada</h1>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.go(-2);\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    protected void mensajeEdit(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>EDIT</title>");
             out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                        "        <style>\n" +
                        "            body {\n" +
                        "                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);\n" +
                        "                font-family: sans-serif;\n" +
                        "            }\n" +
                        "            \n" +
                        "            #divPrincipal {\n" +
                        "                background-color: white;\n" +
                        "                margin-top: 30px;\n" +
                        "                padding: 30px;\n" +
                        "                border-radius: 10px;\n" +
                        "                text-align: center;\n" +
                        "            }\n" +
                        "        </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-5\" id=\"divPrincipal\">");
            out.println("<h1>Edición realizada</h1>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.go(-2);\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}
