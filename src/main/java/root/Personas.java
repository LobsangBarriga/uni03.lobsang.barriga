/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.TblPersonasJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.entity.TblPersonas;

/**
 *
 * @author Karukami
 */
@Path("personas")
public class Personas implements Serializable {
    
    public Personas(){
        
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPersonas(){
        
        TblPersonasJpaController dao = new TblPersonasJpaController();
        List<TblPersonas> lista = dao.findTblPersonasEntities();
        
        return Response.ok(200).entity(lista).build();
        
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearPersona(TblPersonas persona){
        
        try {
            TblPersonasJpaController dao = new TblPersonasJpaController();
            dao.create(persona);
        } catch (Exception ex) {
            Logger.getLogger(TblPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(persona).build();
        
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response borrarPersona(@PathParam("iddelete") String iddelete){
        
        try {
            TblPersonasJpaController dao = new TblPersonasJpaController();
            dao.destroy(iddelete);
        } catch (Exception ex) {
            Logger.getLogger(TblPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Persona " + iddelete + " eliminada").build();
        
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(TblPersonas persona){
        
        try {
            TblPersonasJpaController dao = new TblPersonasJpaController();
            dao.edit(persona);
        } catch (Exception ex) {
            Logger.getLogger(TblPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(persona).build();
        
    }
    
}
