/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.entity.TblPersonas;


/**
 *
 * @author Karukami
 */
public class TblPersonasJpaController implements Serializable {
    
    // ATRIBUTOS
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit"); 

    // CONSTRUCTORES
    public TblPersonasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    public TblPersonasJpaController() {
    }
    
    // GETTER
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblPersonas tblPersonas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        System.out.println("DEBUG: ENTRANDO A FUNCION CREATE DENTRO DEL DAO");
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tblPersonas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTblPersonas(tblPersonas.getRut()) != null) {
                throw new PreexistingEntityException("Personas " + tblPersonas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblPersonas tblPersonas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tblPersonas = em.merge(tblPersonas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tblPersonas.getRut();
                if (findTblPersonas(id) == null) {
                    throw new NonexistentEntityException("The TblPersona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblPersonas tblPersonas;
            try {
                tblPersonas = em.getReference(TblPersonas.class, id);
                tblPersonas.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The TblPersonas with id " + id + " no longer exists.", enfe);
            }
            em.remove(tblPersonas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblPersonas> findTblPersonasEntities() {
        return findTblPersonasEntities(true, -1, -1);
    }

    public List<TblPersonas> findTblPersonasEntities(int maxResults, int firstResult) {
        return findTblPersonasEntities(false, maxResults, firstResult);
    }

    private List<TblPersonas> findTblPersonasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblPersonas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblPersonas findTblPersonas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblPersonas.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblPersonasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblPersonas> rt = cq.from(TblPersonas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
