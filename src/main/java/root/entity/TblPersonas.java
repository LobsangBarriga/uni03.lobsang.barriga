/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Karukami
 */
@Entity
@Table(name = "tbl_personas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblPersonas.findAll", query = "SELECT t FROM TblPersonas t"),
    @NamedQuery(name = "TblPersonas.findByRut", query = "SELECT t FROM TblPersonas t WHERE t.rut = :rut"),
    @NamedQuery(name = "TblPersonas.findByNombre", query = "SELECT t FROM TblPersonas t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TblPersonas.findByApellido", query = "SELECT t FROM TblPersonas t WHERE t.apellido = :apellido"),
    @NamedQuery(name = "TblPersonas.findByDireccion", query = "SELECT t FROM TblPersonas t WHERE t.direccion = :direccion"),
    @NamedQuery(name = "TblPersonas.findByCiudad", query = "SELECT t FROM TblPersonas t WHERE t.ciudad = :ciudad")})
public class TblPersonas implements Serializable {

    @Size(max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 50)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 100)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 50)
    @Column(name = "ciudad")
    private String ciudad;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "rut")
    private String rut;

    public TblPersonas() {
    }

    public TblPersonas(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblPersonas)) {
            return false;
        }
        TblPersonas other = (TblPersonas) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.TblPersonas[ rut=" + rut + " ]";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
}
