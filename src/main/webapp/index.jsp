<%-- 
    Document   : index
    Created on : 18-04-2021, 23:53:21
    Author     : Karukami
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>UNI03 - Lobsang Barriga</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);
                font-family: sans-serif;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5" id="divPrincipal">
                    <h2 style="text-align: center;">- Mantenedor CRUD con API implementada -</h2>
                    <h5 style="text-align: center;">UNI03 - Lobsang Barriga</h5>
                    <hr>
                    <h4>REST Endpoints:</h4><br>
                    <h4>GET</h4>
                    <p>https://lobsangbarrigauni03.herokuapp.com/api/personas</p>
                    <h4>POST</h4>
                    <p>https://lobsangbarrigauni03.herokuapp.com/api/personas</p>
                    <h4>PUT</h4>
                    <p>https://lobsangbarrigauni03.herokuapp.com/api/personas</p>
                    <h4>DELETE</h4>
                    <p>https://lobsangbarrigauni03.herokuapp.com/api/personas/{iddelete}</p>
                    <hr>
                    <form name="miFormulario" action="miControlador" method="POST">
                        <input type="submit" name="btn_jsp_ingresar" value="Ingresar nueva persona" class="btn btn-primary btn-block">
                        <input type="submit" name="btn_jsp_editar" value="Editar personas" class="btn btn-info btn-block">
                        <input type="submit" name="btn_jsp_eliminar" value="Eliminar personas" class="btn btn-danger btn-block">
                        <input type="submit" name="btn_jsp_listar" value="Listar todas las personas" class="btn btn-success btn-block">
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
