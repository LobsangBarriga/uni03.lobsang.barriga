<%-- 
    Document   : eliminar
    Created on : 21-04-2021, 20:41:40
    Author     : Karukami
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Personas</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);
                font-family: sans-serif;
                text-align: center;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                    <div class="col-5" id="divPrincipal">
                        <h1>Eliminar Personas</h1>
                        <form name="frm_eliminacion" action="miControlador" method="POST">
                            <select name="select_eliminar" class="form-control">
                                <c:forEach items="${listaPersonasJsp}" var="persona">
                                    <option value="${persona.rut}">${persona.rut} : ${persona.nombre} ${persona.apellido}</option>
                                </c:forEach>
                            </select>
                            <br>
                            <input type="submit" name="btn_eliminar" value="Confirmar eliminación" class="btn btn-primary btn-block">
                            <input type="button" value="Volver" class="btn btn-info btn-block" onclick="window.history.back();">
                        </form>
                    </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
