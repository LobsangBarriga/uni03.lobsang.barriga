<%-- 
    Document   : editar
    Created on : 22-04-2021, 3:14:46
    Author     : Karukami
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Personas</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);
                font-family: sans-serif;
                text-align: center;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                    <div class="col-5" id="divPrincipal">
                        <h1>Editar Personas</h1>
                        <form name="frm_edicion" action="miControlador" method="POST">
                            <select name="select_editar" class="form-control">
                                <c:forEach items="${listaPersonasJsp}" var="persona">
                                    <option value="${persona.rut}">${persona.rut} : ${persona.nombre} ${persona.apellido}</option>
                                </c:forEach>
                            </select>
                            <br>
                            <div class="form-group row">
                                <label for="txt_edit_nombre" class="col-sm-4 col-form-label">Editar Nombre:</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="Nombre" name="txt_edit_nombre" class="form-control" maxlength="50">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="txt_edit_apellido" class="col-sm-4 col-form-label">Editar Apellido:</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="Apellido" name="txt_edit_apellido" class="form-control" maxlength="50">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="txt_edit_direccion" class="col-sm-4 col-form-label">Editar Dirección:</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="Dirección" name="txt_edit_direccion" class="form-control" maxlength="50">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="txt_edit_ciudad" class="col-sm-4 col-form-label">Editar Ciudad:</label>
                                <div class="col-sm-8">
                                    <input type="text" placeholder="Ciudad" name="txt_edit_ciudad" class="form-control" maxlength="50">
                                </div>
                            </div>
                            <br>
                            <input type="submit" name="btn_editar" value="Confirmar Edición" class="btn btn-primary btn-block">
                            <input type="button" value="Volver" class="btn btn-info btn-block" onclick="window.history.back();">
                        </form>
                    </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
