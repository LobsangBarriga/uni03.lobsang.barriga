<%-- 
    Document   : ingresar
    Created on : 20-04-2021, 23:43:32
    Author     : Karukami
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>UNI03 - Lobsang Barriga</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #9CECFB, #65C7F7, #0052D4);
                font-family: sans-serif;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-5" id="divPrincipal">
                    <h2 style="text-align: center;">Ingresar nueva persona</h2>
                    <h5 style="text-align: center;">UNI03 - Lobsang Barriga</h5>
                    <hr>
                    <form name="miFormulario" action="miControlador" method="POST">
                        <div class="form-group row">
                            <label for="txt_rut" class="col-sm-4 col-form-label">Ingrese RUT:</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="RUT" name="txt_rut" class="form-control" maxlength="10">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="txt_nombre" class="col-sm-4 col-form-label">Ingrese Nombre:</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="Nombre" name="txt_nombre" class="form-control" maxlength="50">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="txt_apellido" class="col-sm-4 col-form-label">Ingrese Apellido:</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="Apellido" name="txt_apellido" class="form-control" maxlength="50">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="txt_direccion" class="col-sm-4 col-form-label">Ingrese Dirección:</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="Dirección" name="txt_direccion" class="form-control" maxlength="50">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="txt_ciudad" class="col-sm-4 col-form-label">Ingrese Ciudad:</label>
                            <div class="col-sm-8">
                                <input type="text" placeholder="Ciudad" name="txt_ciudad" class="form-control" maxlength="50">
                            </div>
                        </div>
                        
                        <input type="submit" name="btn_ingresar" value="Confirmar" class="btn btn-primary btn-block">
                        <input type="button" value="Volver" class="btn btn-info btn-block" onclick="window.history.back();">
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
